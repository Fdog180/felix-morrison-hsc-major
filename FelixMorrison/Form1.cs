﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FelixMorrison
{
    public partial class Level_1 : Form
    {
        //boolean (True/False) which will allow movement including jumping
        bool goleft = false;
        bool goright = false;
        bool jumping = false;

        //integers needed by the logic / "Game Engine" tracks jumpspeed,force,score etc
        int jumpspeed = 10;
        int force = 8;
        int score = 0;

        // this will check the direction the player is facing. On load player faces =>
        string facing = "right";

        // this will check if the player has shot any bullets
        bool shot = false; 


        public Level_1()
        {
            InitializeComponent();
        }

        private void keyisdown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left )
            {
                goleft = true;
                facing = "left";
            }

            if (e.KeyCode == Keys.Right)
            {
                goright = true;
                facing = "right";
            }

            if (e.KeyCode == Keys.Space && !jumping)
            {
                jumping = true;

            }

            if (e.KeyCode == Keys.Z)
                {

                makeBullet();
                shot = true;
            }

        }



        private void keyisup(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Left)
            {
                goleft = false;
            }

            if (e.KeyCode == Keys.Right)
            {
                goright = false;
            }

            if (e.KeyCode == Keys.Space && !jumping)
            {
                jumping = false;
            }

            if (shot == true)
            {
                //if shot variable is true, I change it to false to force player to shoot again.
                  shot = false;
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            player.Top += jumpspeed;

            if (jumping && force < 0)
            {
                jumping = false;

            }

            if (goleft)
            {
                player.Left -= 4;
            }
            if (goright)
            {
                player.Left += 4;
            }
            if (jumping)
            {
                jumpspeed = -12;
                force -= 1;
            }
            else
            {
                jumpspeed = 12;
            }
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && x.Tag == "platform")
                {

                    if (player.Bounds.IntersectsWith(x.Bounds) && !jumping)
                    {

                        force = 8;
                        player.Top = x.Top - player.Height;
                    }


                }

                if (x is PictureBox && x.Tag == "coin")
                {

                    if (player.Bounds.IntersectsWith(x.Bounds))

                    {
                        this.Controls.Remove(x);
                        score++;
                        //score_count.Text = (int.Parse(score_count.Text) + 1).ToString();
                        score_count.Text = Convert.ToString(score);
                    }

                }

                if (x is )


            }

            if (player.Bounds.IntersectsWith(door.Bounds))
            {

                timer1.Stop();
                DialogResult d = MessageBox.Show("You Win!", "Level 2", MessageBoxButtons.YesNo);
                if (d == DialogResult.Yes)
                {
                    this.Hide();
                    var Form2 = new Level_2();
                    Form2.Show();

                }


            }


            foreach (Control X in this.Controls)
            {
                // if X is a picture box object AND it has a tag of bullet
                // then we will follow the instructions within
                if (X is PictureBox && X.Tag == "bullet")
                {

                   
                        // move x towards the right of the screen
                        X.Left += 15;
                        // if x has left the screen towards the right
                        // x's location is greater than 900 pixels from the screen
                        if (X.Left > 900) 
                        {
                            // then remove x from display
                            this.Controls.Remove(X);
                            // dispose the x from the application
                            // we use the dispose method so it doesn't leak memory later on
                            X.Dispose();
                        }

                                                       
                   

                    
                }



            }
        }


        private void makeBullet()
        {
            //This makes a bullet instead of having one on there form and then duplicated. It's a bit cleaner
            PictureBox bullet = new PictureBox();
            bullet.BackColor = System.Drawing.Color.DarkOrange;
            bullet.Height = 5;
            bullet.Width = 10;
            bullet.Left = player.Left + player.Width;
            bullet.Top = player.Top + player.Height / 2;
            bullet.Tag = "bullet";
            this.Controls.Add(bullet);
        }

        private void Load(object sender, EventArgs e)
        {
            var Form2 = new Level_2();
            var form1 = new Level_1();
            Form2.Show();        }

        }
    }

